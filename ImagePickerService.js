//plugin dependency: https://github.com/wymsee/cordova-imagePicker.git
angular.module('starter.services')
.factory('ImagePicker', function($rootScope, $cordovaImagePicker, $cordovaCamera, $ionicActionSheet, $q){
	var options = {
		maximumImagesCount:1,
		width: 1200,
		height: 0,
		quality: 90,
	};
	
	var setDefaultWidth = function(w){
		options.width = parseInt(w);
	};
	
	var setDefaultHeight = function(h){
		options.height = parseInt(h);
	};
	
	var setDefaultQuality = function(q){
		options.quality = parseInt(q);
	};
	
	var getOptions = function(){
		return options;
	};
	
	var pickFromGallery = function(){
		return $cordovaImagePicker.getPictures(options);
	};
	
	var pickFromCamera = function(){
		var cameraOptions = {
			quality: getOptions().quality,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: Camera.PictureSourceType.CAMERA,
			encodingType: Camera.EncodingType.JPEG,
			popoverOptions: CameraPopoverOptions,
			allowEdit: false,
			saveToPhotoAlbum: false,
			correctOrientation: true,
			targetWidth: getOptions().width,
			targetHeight: getOptions().height,
		};
		
		return $cordovaCamera.getPicture(cameraOptions).then(function(imageData){
			if( !/data:image\/*.;base64/.test(imageData) ){
				imageData = 'data:image/jpeg;base64,'+imageData;
			}
			
			return imageData;
		});
	};
	
	var pickWithSourceOptions = function(aditionalButtons, customButtonCallback){
		var deferred = $q.defer();
		var buttonsList = [
			{ text: "Pick from gallery" },
			{ text: "Pick from camera" },
		];
		
		if(aditionalButtons && aditionalButtons.length>0){
			for(var i in aditionalButtons){
				buttonsList.push({
					text: aditionalButtons[i],
				});
			}
		}
		
		var hideSheet = $ionicActionSheet.show({
			buttons: buttonsList,
			titleText: "Pick Image",
			destructiveText: "Cancel",
			cancelText: "Cancel",
			cancel: function() {
				deferred.reject('canceled');
				return true;
			},
			destructiveButtonClicked: function() {
				deferred.reject('destroyed');
				return true;
			},
			buttonClicked: function(index) {
				if( index===0 ){
					//PICK FROM GALLERY
					pickFromGallery().then(
						function(results){
							if( results.length>0 ){
								var uri = results[0];
								deferred.resolve(uri);
							}else{
								deferred.reject(results);
							}
						},
						function(error){
							deferred.reject(error);
						}
					);
					//$cordovaImagePicker.getPictures(options);
				}else if( index==1 ){
					//PICK FROM CAMERA
					pickFromCamera().then(
						function(imageData){
							if( !/data:image\/*.;base64/.test(imageData) ){
								deferred.resolve(imageData);
							}else{
								deferred.reject(imageData);
							}
						},
						
						function(error){
							deferred.reject(error);
						}
					);
				}else{
					if( buttonsList[index] && typeof customButtonCallback=='function' ){
						var response = customButtonCallback.apply(this, [buttonsList[index], index-2]);
						
						if( response!=null && typeof response!='undefined' ){
							deferred.resolve(response);
						}else{
							deferred.reject('custom button '+index+' empty response');
						}
					}else{
						deferred.reject('unknow');
					}
				}
				
				return true;
			}
	  	});
	  	
	  	return deferred.promise;
	};
	
	return {
		pickFromGallery: pickFromGallery,
		pickFromCamera: pickFromCamera,
		pickWithSourceOptions: pickWithSourceOptions,
		setDefaultWidth: setDefaultWidth,
		getOptions: getOptions,
	};
})
